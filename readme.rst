ZED cython wrapper
==================

Introduction
------------

ZED is Stereolab_ stereo camera. 
This repository has cython bindings for the ZED sdk. It is
based on the cython-cmake-example_ code.

The project is tested on the *Jetson-TK1* development board. The bindings are
still in development and expose only part of the ZED SDK.

Dependencies
------------

Build Dependencies
^^^^^^^^^^^^^^^^^^

- Python_
- Cython_
- CMake_
- C++ compiler (g++ for instance)
- `ZED SDK`_
- `CUDA SDK`_
- `OpenCV`_ and its python bindings

Build Instructions
------------------

::

  mkdir build
  cd build
  cmake ..
  make
  python setup.py install
  
.. warning::

  In your CMake configuration, make sure that PYTHON_LIBRARY,
  PYTHON_INCLUDE_DIR, and CYTHON_EXECUTABLE are all using the same CPython
  version.

Basic Usage
-----------
See the examples in the folder 'examples'.

License
-------

See included license file.

.. _Stereolab: https://www.stereolabs.com/
.. _cython-cmake-example: https://github.com/thewtex/cython-cmake-example
.. _Cython: http://cython.org/
.. _CMake:  http://cmake.org/
.. _Python: http://python.org/
.. _ZED SDK: https://www.stereolabs.com/developers/
.. _CUDA SDK: https://developer.nvidia.com/cuda-downloads
.. _OpenCV: http://opencv.org/

Issues Encountered
------------------

1. Cmake failure due to missing Eigen/Core

::

  jetson@tegra-ubuntu:~/Code/zed-cython$ cd build
  jetson@tegra-ubuntu:~/Code/zed-cython/build$ cmake ..
  -- Configuring done
  -- Generating done
  -- Build files have been written to: /home/jetson/Code/zed-cython/build
  jetson@tegra-ubuntu:~/Code/zed-cython/build$ make
  [  0%] Built target ReplicatePythonSourceTree
  [ 50%] Building CXX object src/CMakeFiles/_zed.dir/_zed.cxx.o
  In file included from /home/jetson/Code/zed-cython/build/src/_zed.cxx:452:0:
  /usr/local/zed/include/zed/Camera.hpp:73:22: fatal error: Eigen/Core: No such file or directory
  #include <Eigen/Core>
                      ^
  compilation terminated.
  make[2]: *** [src/CMakeFiles/_zed.dir/_zed.cxx.o] Error 1
  make[1]: *** [src/CMakeFiles/_zed.dir/all] Error 2
  make: *** [all] Error 2

To solve 
  I had to update ```/usr/local/zed/include/zed/Camera.hpp``` 
  
  ::
  
    #include <Eigen/Core> 
    #CHANGE TO
    #include <eigen3/Eigen/core>

2. import cv2 could not be found

To solve 
  I had to run `sudo apt-get install libopencv4tegra-python`

3. Make failure due to previous declaration

::

  /home/jetson/Code/zed-cython/build/src/_zed.cxx:16408:27: error: using typedef-name \u2018sl::zed::COORDINATE_SYSTEM\u2019 after \u2018enum\u2019
     return (enum sl::zed::COORDINATE_SYSTEM) -1;
                           ^
  In file included from /usr/local/zed/include/zed/Mat.hpp:71:0,
                 from /usr/local/zed/include/zed/Camera.hpp:70,
                 from /home/jetson/Code/zed-cython/build/src/_zed.cxx:420:
  /usr/local/zed/include/zed/utils/GlobalDefine.hpp:424:11: note: \u2018sl::zed::COORDINATE_SYSTEM\u2019 has a previous declaration here
         } COORDINATE_SYSTEM;
           ^

To solve 
  I had to modify the ```/usr/local/include/zed/utils/GlobalDefine.hpp``` by adding the additional enum naming for each enum throwing errors.  For example, I changed ```typedef enum``` to ```typedef enum COORDINATE_SYSTEM```.
  
  ::
    
        typedef enum COORDINATE_SYSTEM {
            IMAGE = 1, /*!< standard coordinate system in computer vision. Used in OpenCV : see here : http://docs.opencv.org/2.4/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html */
            LEFT_HANDED = 2, /*!< Same as IMAGE but with Y axis inverted (defined by the left-hand rule). Used in Unity or DirectX */
            RIGHT_HANDED = 4, /*!< Same as LEFT_HANDED but with Z axis inverted (defined by the right-hand rule). Used in OpenGL */
            ROBOTIC = 8, /*!< specific ROBOTIC coordinate system : right-hand rule with Z pointing up and Y forward. */
                        APPLY_PATH = 16, /*!< Set it to apply the path to the point cloud. */
            LAST_COORDINATE_SYSTEM = 32
        } COORDINATE_SYSTEM;

  
  

