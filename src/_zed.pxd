from libcpp.string cimport string
from libcpp cimport bool

cdef extern from "zed/Camera.hpp" namespace "sl::zed":

    ctypedef unsigned char uchar

    cpdef enum ERRCODE:
        SUCCESS
        NO_GPU_COMPATIBLE
        NOT_ENOUGH_GPUMEM
        ZED_NOT_AVAILABLE
        ZED_RESOLUTION_INVALID
        ZED_SETTINGS_FILE_NOT_AVAILABLE
        INVALID_SVO_FILE
        RECORDER_ERROR
        INVALID_COORDINATE_SYSTEM
        ZED_WRONG_FIRMWARE
        NO_NEW_FRAME
        CUDA_ERROR_THROWN
        ZED_NOT_INITIALIZED
        OUT_OF_DATE_NVIDIA_DRIVER
        INVALID_FUNCTION_CALL
        LAST_ERRCODE

    cpdef enum MODE:
        NONE
        PERFORMANCE
        MEDIUM
        QUALITY
        LAST_MODE

    cpdef enum VIEW_MODE:
        STEREO_ANAGLYPH
        STEREO_DIFF
        STEREO_SBS
        STEREO_OVERLAY
        LAST_VIEW_MODE

    cpdef enum SIDE:
        LEFT
        RIGHT
        LEFT_GREY
        RIGHT_GREY
        LEFT_UNRECTIFIED
        RIGHT_UNRECTIFIED
        LEFT_UNRECTIFIED_GREY
        RIGHT_UNRECTIFIED_GREY
        LAST_SIDE

    cpdef enum ZEDResolution_mode:
        HD2K
        HD1080
        HD720
        VGA
        LAST_RESOLUTION

    cpdef enum MEASURE:
        DISPARITY
        DEPTH
        CONFIDENCE
        XYZ
        XYZRGBA
        XYZBGRA
        XYZARGB
        XYZABGR
        LAST_MEASURE

    cpdef enum SENSING_MODE:
        FILL
        STANDARD 
        LAST_SENSING_MODE

    cpdef enum COORDINATE_SYSTEM:
        IMAGE
        LEFT_HANDED
        RIGHT_HANDED
        ROBOTIC
        APPLY_PATH
        LAST_COORDINATE_SYSTEM

    cpdef enum UNIT:
        MILLIMETER
        METER
        INCH
        FOOT
        LAST_UNIT

    ctypedef enum DATA_TYPE:
        FLOAT
        UCHAR

    ctypedef enum MAT_TYPE:
        CPU
        GPU

    cdef cppclass Mat:
        Mat()
        Mat(int, int, int, DATA_TYPE, uchar*, MAT_TYPE)
        uchar* getValue(int, int)
        void setUp(int, int, int, uchar*, int, DATA_TYPE, MAT_TYPE)
        void setUp(int, int, int, DATA_TYPE, MAT_TYPE)
        void allocate_cpu(int, int, int, DATA_TYPE)
        void allocate_cpu()
        void deallocate()
        int getWidthByte()
        int getDataSize()
        void info()
        int width
        int height
        int step
        int channels
        uchar* data
        DATA_TYPE data_type

    cdef cppclass resolution:
        resolution(int, int)
        int width, height

    cdef struct InitParams:
        MODE mode
        UNIT unit
        COORDINATE_SYSTEM coordinate
        bool verbose
        int device
        float minimumDistance
        bool disableSelfCalib
        bool vflip

    cdef cppclass Camera:
        Camera(string) except +
        Camera(ZEDResolution_mode, float, int) except +
        resolution getImageSize()
        ERRCODE init(InitParams &parameters)
        bool grab(SENSING_MODE, bool, bool, bool)
        Mat retrieveImage(SIDE)
        Mat retrieveMeasure(MEASURE)
        Mat normalizeMeasure(MEASURE, float, float)
        int getDepthClampValue()
        void setDepthClampValue(int)
        Mat getView(VIEW_MODE)
